This plugin tests headers or ```SpamassassinReport``` for given regex pattern.
```header``` can be provided to be appended to message upon hit of ```pattern```


See ```header_tests.conf.dist``` for config options and rename it to ```header_tests.conf``` after setting the appropriate values.

```
checks=prefix prefix1 prefix2
returnonhit=True
```
 * **checks** defines names of tests to run
 * **returnonhit** <boolean> defines if plugin should return after first hit DEFAULT: **True**

The tests above are the prefix to find the corresponding config options which contain the following
 * **prefix_regex** <regex> to apply on header or spamreport
 * **prefix_action** <string> as return value for the test if set to `virus` the message will be marked as virus by fuglu
 * **prefix_headername** <string> which header to test with regex DEFAULT: **SAPlugin.report**
 * **prefix_header2write** <string> which header to append upon match
 * **prefix_header2writeValue** <string> which value for the header to append
 * **prefix_returnonhit** <boolean> return after sucessfull test DEFAULT: **True**
 * **prefix_internalheader** <boolean> header will only be appended internally for fuglu DEFAULT: **False**

except regex and headername the other options are non-mandatory
 * if **headername** is given and **regex** is not the existence of the header is enough for fullfill the test
 * if **regex** is given but **headername** is not then regex is applied to default of headername


