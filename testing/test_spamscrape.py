# -*- coding: UTF-8 -*-
import unittest
from exp_filters.spamscrape import SpamScrape
from fuglu.shared import Suspect, FuConfigParser


class TestSpamSubjectMixing(unittest.TestCase):
    def setUp(self) -> None:
        config = FuConfigParser()
        config.add_section("SpamScrape")
        config.set(section="SpamScrape", option="senderdomainregex", value='^(outlook\.|hotmail\.)')
        config.set(section="SpamScrape", option="badtlds",
                   value='art best bid buzz cam center cf city click club cyou date '
                         'design diet faith fit fun ga gdn gq guru host icu life live '
                         'men ml money monster online ooo press pro rest review site '
                         'space stream study su tech tk today top trade wang website '
                         'work world xyz')

        self.candidate = SpamScrape(config)

    def test_sender_regex_nohit(self,):
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', "/dev/null")
        self.assertFalse(self.candidate._process_sender(suspect))

    def test_sender_regex_hit(self, ):
        suspect = Suspect("from@outlook.com", 'recipient@unittests.fuglu.org', "/dev/null")
        self.assertTrue(self.candidate._process_sender(suspect))

    def test_baduri(self, ):
        suspect = Suspect("from@outlook.com", 'recipient@unittests.fuglu.org', "/dev/null")
        cleanuris = ["fuglu.org"]
        baduris = ["redirect.xyz"]
        suspect.set_tag("body.uris", baduris + cleanuris)

        _ = self.candidate.examine(suspect)

        self.assertTrue(suspect.is_spam())

        scrapeuris = suspect.get_tag('scrape.uris')
        self.assertEqual(scrapeuris, baduris)


class TestSpamSubjectMixingLogOnly(unittest.TestCase):
    def setUp(self) -> None:
        config = FuConfigParser()
        config.add_section("SpamScrape")
        config.set(section="SpamScrape", option="log_only", value='true')
        config.set(section="SpamScrape", option="senderdomainregex", value='^(outlook.|hotmail.)')
        config.set(section="SpamScrape", option="badtlds",
                   value='art best bid buzz cam center cf city click club cyou date '
                         'design diet faith fit fun ga gdn gq guru host icu life live '
                         'men ml money monster online ooo press pro rest review site '
                         'space stream study su tech tk today top trade wang website '
                         'work world xyz')

        self.candidate = SpamScrape(config)

    def test_baduri(self, ):
        """Log only mode, no tags or marking"""
        suspect = Suspect("from@outlook.com", 'recipient@unittests.fuglu.org', "/dev/null")
        baduris = ["redirect.xyz"]
        suspect.set_tag("body.uris", baduris)

        _ = self.candidate.examine(suspect)

        self.assertFalse(suspect.is_spam())

        scrapeuris = suspect.get_tag('scrape.uris', [])
        self.assertEqual(scrapeuris, [])
