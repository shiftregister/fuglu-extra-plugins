# -*- coding: UTF-8 -*-
import unittest
import logging
import sys
import os
from fuglu.stringencode import force_uString
from exp_filters.btc import BTCCounter
from fuglu.shared import Suspect, FuConfigParser
from testing.storedmails import mailsdir

def setup_module():
    root = logging.getLogger()
    root.setLevel(logging.DEBUG)
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    root.addHandler(handler)


def get_config():
    confdefaults = b"""
[test]
redisdb = redis://127.0.0.1:6379/1
btcblacklistfile = ''
"""
    conf = FuConfigParser()
    conf.read_string(force_uString(confdefaults))
    return conf


BTC_MAIL = [
#    ('btc_html_b64.eml', '12yCNJHAwda8Kgxv9DswpS9k16XnstSqcJ'),
#    ('btc_scrambled.eml', '1A5Kr9Up2sMp3fWeVVrzTHPEfvoinCGE6c'),
#    ('btc_scrambled2.eml', '13KRkTwEvd8suAjK389Qu28XWLCsPXvNKj'),
#    ('btc_regular_b64.eml', '1M3r1T2MnqRPJUgWoSZTgaTqpVQUNdkpeC'),
#    ('btc_regular_plain.eml', '18p86MxfVKdTn5FhxU97WHJ3af1fHpCnfn'),
#    ('btc_scrambled3.eml', '1JTGgvEBtXfveMGhgHFKbY6YSVJs5vvscd'),
    ('btc_bc1.eml', 'bc1q7y8nhq6j237hcdup0ve2vatcrxc2x9uwc5d4tz'),
]


class TestBTCMixing(unittest.TestCase):
    def test_all_btc(self):
        for filename, btcaddr in BTC_MAIL:
            self._test_btc(filename, btcaddr)


    def _test_btc(self, filename, btcaddr):
        """Test mail subject with "ä" but not encoded"""
        conf = get_config()

        filename = os.path.join(mailsdir, filename)
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org',filename)

        btcm = BTCCounter(conf, 'test')
        btcaddrs = btcm.extract(suspect)

        #print(btcaddrs)
        self.assertIn(btcaddr, btcaddrs, 'BTC address %s not found in result' % btcaddr)
