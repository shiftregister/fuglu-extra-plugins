# -*- coding: utf-8 -*-

from fuglu.shared import ScannerPlugin,DUNNO,FileList
import os
import time
import random


class Detach(ScannerPlugin):
    def __init__(self,config,section=None):
        ScannerPlugin.__init__(self,config,section)
        self.logger=self._logger()
        
        self.requiredvars = {
    
            'skipsdr': {
                'default': '',
                'description': "comma separated list of senders that can be ignored",
            },
            
            'usercpt': {
                'default': '',
                'description': "comma separated list of recipients for which to run this plugin",
            },
    
            'savedir': {
                'default': '/usr/local/fuglu/spamreport/new',
                'description': "save mail to this directory. must haver permission 777 and be owned by user/group set by saveuser/savegroup",
            },
            
            'saveuser':{
                'default':'vmail',
                'description':"save mail with permissions of this user",
            },
            
            'savegroup':{
                'default':'vmail',
                'description':"save mail with permssions of this group",
            },
            
            'skiplist': {
                'default': '/etc/fuglu/detach_skiplist.txt',
                'description': 'path to skiplist file, contains one skippable subject per line'
            },
        }
        
        self.skiplist = None
    
    
    
    def _init_skiplist(self):
        if self.skiplist is None:
            filepath = self.config.get(self.section, 'skiplist')
            if filepath and os.path.exists(filepath):
                self.skiplist = FileList(filename=filepath)
    
    
    
    def examine(self,suspect):
        usercpt = [s.strip().lower() for s in self.config.get(self.section, 'usercpt').split(',')]
        if suspect.to_address.lower() not in usercpt:
            return DUNNO
        
        skipsdr = [s.strip().lower() for s in self.config.get(self.section, 'skipsdr').split(',')]
        if suspect.from_address.lower() in skipsdr:
            return DUNNO
        
        msgrep = suspect.get_message_rep()
        if not msgrep.is_multipart():
            return DUNNO
        
        self._init_skiplist()
        skiplist = self.skiplist.get_list()
        if skiplist:
            subject = msgrep.get('Subject', '')
            if subject in skiplist:
                return DUNNO
        
        for i in msgrep.walk():
            contenttype_mime=i.get_content_type()
            if contenttype_mime is not None and contenttype_mime.lower() in ['message/rfc822',]:
                payload = i.get_payload(0)
                payload = payload.as_bytes()
                startofmsg=payload[:10000]
                if b'Received: ' in startofmsg or b'Subject: ' in startofmsg or b"From: " in startofmsg:
                    self.save_attachment(payload, suspect)
                else:
                    self.logger.warning("%s attached rfc822 message from %s seems to be broken (headers not found).. ignoring" % (suspect.id, suspect.from_address))
                    
        return DUNNO
    
    
    def lint(self):
        ok = self.check_config()
        if ok:
            self._init_skiplist()
            if not self.skiplist:
                print('WARNING: failed to initialise skiplist')
                ok = False
        if ok:
            path = self.config.get(self.section, 'savedir')
            if not os.path.exists(path):
                print("ERROR: Target folder does not exist: %s" % path)
        return ok
    

    @staticmethod
    def create_attachment_filename(fromdomain):
        filename = "%s-%s-%s.eml" % (fromdomain, time.time(), random.randint(1, 10000))
        return filename

    
    def save_attachment(self, payload, suspect):
        path = self.config.get(self.section, 'savedir')
        if not os.path.exists(path):
            self.logger.error("Target folder does not exist: %s" % path)
            return
        filename = Detach.create_attachment_filename(suspect.from_domain)

        fullpath=os.path.join(path, filename)
        
        success = True
        try:
            with open(fullpath, 'wb', 0o0666) as fh:
                fh.write(payload)
            self.logger.info("%s attached message from %s saved as %s" % (suspect.id, suspect.from_address, fullpath))
        except Exception as e:
            success = False
            self.logger.error("%s failed to save attached message from %s as %s"%(suspect.id, suspect.from_address,fullpath))
            self.logger.exception(e)

        if success:
            try:
                os.chmod(fullpath, 0o0666)
            except Exception:
                success = False
                self.logger.error("%s failed to change permission of %s"%(suspect.id, fullpath))
                
        return success
        
        
        
        
