#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#   Copyright 2009-2021 Fumail Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
#
__version__ = '0.3'

import re
import sys
import time
import email
from email.header import decode_header
import traceback
import ssdeep
import threading
import os
import logging

#This is not really used in fuglu, it's just a dummy implementation compatible with other fuglu AV scanners so we can use it the rescanattachments.py script easily

# to prevent code repetition use modules from fuglu
# (which actually means this can not be run anymore without fuglu installation and the fuglu source directory in the PYTHON_PATH)
from fuglu.shared import AVScannerPlugin, SuspectFilter,DUNNO
from fuglu.extensions.sql import get_session
from fuglu.stringencode import force_uString
from fuglu.lib.patchedemail import PatchedMessage
from fuglu.mailattach import Mailattachment_mgr, NoExtractInfo


MAX_MEM_EXTRACT=5000000 # extract files up to this amount of bytes into memory


def asciionly(stri):
    """return stri with all non-ascii chars removed"""
    if sys.version_info > (3,):
        if isinstance(stri, str):
            return stri.encode('ascii', 'ignore').decode()
        elif isinstance(stri, bytes):  # python3
            # A bytes object therefore already ascii, but not a string yet
            return stri.decode('ascii', 'ignore')
    return "".join([x for x in stri if ord(x) < 128])


class SSDEEPCheck(AVScannerPlugin):
    """Submit attachments in virus infected messages to atthash"""
    
    def __init__(self, config, section=None):
        AVScannerPlugin.__init__(self, config, section)
        
        self.requiredvars = {
            'dbconnectstring':{
                'default':'mysql://root@localhost/ssdeep',
                'description': 'database url',
            },
            
            'maxsize': {
                'default': '22000000',
                'description': "maximum message size, larger messages will not be scanned.  ",
            },

            'threshold':{
                'default':'75',
                'description': 'ssdeep lock threshold',
            },
            
            'minattachmentsize':{
                'default':'5',
                'description': 'minimum attachment size',
            },
            
            'maxattachmentsize':{
                'default':'5000000',
                'description': 'maximum attachment size',
            },
            
            'virusaction': {
                'default': 'DEFAULTVIRUSACTION',
                'description': "action if infection is detected (DUNNO, REJECT, DELETE)",
            },

            'archivecontentmaxsize': {
                'default': '5000000',
                'description': 'only extract and examine files up to this amount of (uncompressed) bytes',
            },

            'problemaction': {
                'default': 'DEFER',
                'description': "action if there is a problem (DUNNO, DEFER)",
            },

            'rejectmessage': {
                'default': 'threat detected: ${virusname}',
                'description': "reject message template if running in pre-queue mode and virusaction=REJECT",
            },
            
            'filenamesrgx': {
                'default': '\.(exe|scr|com|pif)$',
                'description': 'regex of filenames to be hashed',
            },
        }
        self.logger = self._logger()
        self.enginename = 'ssdeep'


    def __str__(self):
        return "SSDEEP"


    def examine(self,suspect):
        if self._check_too_big(suspect):
            return DUNNO

        content = suspect.get_source()

        try:
            viruses = self.scan_stream(content, suspect.id)
            actioncode, message = self._virusreport(suspect, viruses)
            return actioncode, message
        except Exception as e:
            self.logger.warning("%s Error encountered while checking ssdeep: %s" % (suspect.id, str(e)))

        return self._problemcode()



    def scan_stream(self, content, suspectid='(NA)', attachmentmanager=None):

        dr = dict()

        if attachmentmanager:
            # if sent in, use attachment manager since it has all information and files already
            # extracted and buffered
            attmanager = attachmentmanager
        else:
            # otherwise create the manager from the message
            try:
                msg = email.message_from_string(content, _class=PatchedMessage)
            except TypeError:
                msg = email.message_from_bytes(content, _class=PatchedMessage)
            attmanager = Mailattachment_mgr(msg, suspectid)

        for att in attmanager.get_objectlist():
            att_name = att.filename
            if att.filename_generated:
                # if filename was auto-generated, set to None to be consistent with old implementation
                att_name = None

            payload = att.buffer
            att_len = len(payload)
            self.logger.debug("%s check %s" % (str(suspectid), att_name))
            if att_len < self.config.getint(self.section, 'minattachmentsize'):
                continue

            if att_len > self.config.getint(self.section, 'maxattachmentsize'):
                continue

            if not att.is_archive:  # no mime-type or filename regex matched the current attachment
                self.logger.debug('hashme is false -> skip')
                continue
            
            try:
                self.logger.debug('handle_archive')
                filename, score = self.handle_archive(att)
                if filename is not None:
                    key = u"%s->%s" % (asciionly(att_name) if att_name is not None else att_name, asciionly(filename))
                    value = u"CM.zeroday.SSD/sc%s" % score
                    dr[key] = value
                    return dr
            
            except Exception as e:
                message = str(e)
                if 'password' in message:
                    pass  # hide error message due to encrypted archive
                elif 'compression type' in message:
                    pass  # hide error message due to unsupported pkzip type 9 (deflate64) etc
                elif 'Bad magic number' in message:
                    pass  # hide errors from invalid zip files
                else:
                    #self.logger.error("could not handle attachment: ")
                    self.logger.debug(traceback.format_exc())
                    raise
        return None
    

    def handle_archive(self, att):
        """Return the known filename with the hightest hitrate (above the configured threshold or None if no file reaches configured threshold"""

        att_name = att.filename
        found_filename = None
        found_score = 0

        rgx = re.compile(self.config.get(self.section, 'filenamesrgx'))
        session = get_session(self.config.get(self.section, 'dbconnectstring'))

        res=session.execute("SELECT ssdeep,filename FROM hashinfo")
        known_hashes = {}
        for row in res:
            known_hashes[row[0]] = row[1]
        
        if att.archive_handle is None:
            self.logger.info('%s invalid archive?' % att_name)

        namelist = att.fileslist_archive

        for name in namelist:
            if not re.search(rgx, name):
                self.logger.debug("name: %s not matching pattern: %s"
                                  % (name, rgx.pattern))
                continue

            noextractinfo = NoExtractInfo()
            extracted_object = att.get_archive_obj(name, MAX_MEM_EXTRACT, noextractinfo=noextractinfo)
            if not extracted_object:
                for item in noextractinfo.get_filtered():
                    try:
                        filename, message = item
                        self.logger.info("file %s in archive %s failed to extract. reason: %s"
                                         % (asciionly(name), asciionly(att_name), message))
                    except Exception as e:
                        self.logger.info("file %s in archive %s failed to extract. reason: %s"
                                         % (asciionly(name), asciionly(att_name), str(e)))
                continue

            extracted = extracted_object.buffer
            sshash = ssdeep.hash(extracted)
            self.logger.debug("sshash: %s     filename: %s" % (sshash, asciionly(name)))

            for knownhash, filename in iter(known_hashes.items()):
                try:
                    score = ssdeep.compare(knownhash, sshash)
    
                    if score >= self.config.getint(self.section, 'threshold') and score > found_score:
                        found_score = score
                        found_filename = filename
                except ssdeep.InternalError as e:
                    self.logger.error('failed to compare ssdeep hashes. error=%s knownhash=%s sshash=%s' % (str(e), knownhash, sshash))

        return found_filename, found_score
    
    

class SSDEEPBody(AVScannerPlugin):
    """Build """

    def __init__(self, config, section=None):
        AVScannerPlugin.__init__(self, config, section)
        self.filter=SuspectFilter(None)
        self.requiredvars = {
            'hashfile':{
                'default':'/tmp/ssdeephashes.txt',
                'description': 'database url',
            },
            'maxsize': {
                'default': '22000000',
                'description': "maximum message size, larger messages will not be scanned.  ",
            },

            'mintextpartsize': {
                'default': '100',
                'description': "minimum text part size to be considered for ssdeep ",
            },
             'maxtextpartsize': {
                'default': '22000000',
                'description': "maximum text part size to be considered for ssdeep ",
            },
            'mode':{
                'default':'loghash',
                'description': 'loghash: just output hash, logscore: log hash and current score, header: write header if hit is detected, write: append to the database(trap server)',
            },
            'threshold':{
                'default':'75',
                'description': 'ssdeep threshold. mode write: append to db if below. other modes: perform action if above',
            },

        }
        self.logger = self._logger()
        self.knownhashes={}
        self.lastreload=0
        self.lock=threading.Lock()


    def __str__(self):
        return "SSDEEP Body"


    def examine(self,suspect):
        if suspect.size > self.config.getint(self.section, 'maxsize'):
            self.logger.info('Not scanning - message too big')
            return

        msg = suspect.get_message_rep()
        textparts = self.filter.get_decoded_textparts(suspect)

        self.logger.debug("Number of text parts in mail: %u"%len(textparts))
        #fallback to full body if no declared text part
        if len(textparts)==0:
            textparts=[msg.get_payload(decode=True)]
            self.logger.debug("No text part, read the full body")

        #find the largest text part within the configured bounds
        candidate=None
        largest=0
        for part in textparts:
            if part is None:
                continue
            partlen=len(part)
            #self.logger.info("prtlen %s"%partlen)
            if partlen<self.config.getint(self.section,'mintextpartsize') or partlen>self.config.getint(self.section,'maxtextpartsize'):
                continue
            if partlen<=largest:
                continue
            candidate=part
            largest=partlen


        if candidate is None:
            self.logger.info("SSDEEP body check: no suitable textpart found")
            return
        sshash=ssdeep.hash(candidate)
        filename='messagebody'
        subject=msg.get('subject',None)
        if subject is not None:
            try:
                filename="".join(x[0] for x in decode_header(subject))
                filename=asciionly(filename).strip()
                if len(filename)<1:
                    filename='messagebody'
            except Exception:
                pass


        mode = self.config.get(self.section,'mode')
        if mode=='loghash':
            self.logger.info("SSDEEP body check: text part=%s bytes, ssdeep=%s identifier=%s"%(largest,sshash,filename))
            return
        self.load_if_necessary()


        found_score=0
        for knownhash,knownfilename in iter(self.knownhashes.copy().items()):
            score=ssdeep.compare(knownhash,sshash)
            #self.logger.info('score against %s = %s'%(knownhash,score))
            if score>found_score:
                found_score=score
            if score>99:
                break

        self.logger.info("SSDEEP body check: score=%s text part=%s bytes, ssdeep=%s identifier=%s"%(found_score,largest,sshash,filename))
        suspect.add_header('X-SSDEEP-Score',str(found_score))
        if mode=='logscore':
            return

        threshold = self.config.getint(self.section,'threshold')
        if mode=='write' and found_score<=threshold:
            self.write(sshash,filename)

    def write(self,sshash,filename):
        self.knownhashes[sshash]=filename
        hashfile = self.config.get(self.section,'hashfile')
        gotlock=self.lock.acquire(False)
        if gotlock:
            with open(hashfile, 'a') as f:
                f.write('%s\t%s\n'%(sshash,filename))
            self.lock.release()

    def load_if_necessary(self):
        hashfile=self.config.get(self.section,'hashfile')
        if not os.path.exists(hashfile):
            return

        if self.config.get(self.section,'mode')=='write':
            if time.time()-self.lastreload>300:  #reload every 5 min
                self.load_db()
        else:
            filetime=os.stat(hashfile).st_mtime
            self.logger.info("filetime=%s lastreload=%s"%(filetime,self.lastreload))
            if self.lastreload<filetime:
                self.load_db()

    def load_db(self):
        gotlock=self.lock.acquire(False)
        if not gotlock:
            return #another thread is loading
        hashfile=self.config.get(self.section,'hashfile')
        self.logger.info("Reloading %s"%hashfile)
        self.lastreload=time.time()
        newdb={}
        with open(hashfile, 'r') as f:
            for line in f.readlines():
                line=line.strip()
                if line=='' or line.startswith('#'):
                    continue
                try:
                    sshash,filename=line.split('\t',1)
                    newdb[sshash]=filename
                except:
                    self.logger.warning("ignoring line: %s"%line)
        self.knownhashes=newdb
        self.logger.info("Got %s hashes from %s"%(len(newdb),hashfile))
        self.lock.release()



if __name__ == '__main__':
    try:
        from unittest.mock import patch
        from unittest.mock import MagicMock
    except ImportError:
        from mock import patch
        from mock import MagicMock


    def get_session_mock(*a, **kw):
        # mock the session object
        m = MagicMock()
        # and the execute function of the session object
        m.execute.return_value = []
        return m


    #--------#
    # logger #
    #--------#
    root = logging.getLogger()
    root.setLevel(logging.DEBUG)
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    root.addHandler(handler)

    path_filename=os.path.abspath(__file__)
    path, fn = os.path.split(path_filename)
    loggername = "(%s) %s" % (fn,__name__)
    logger = logging.getLogger(loggername)

    if len(sys.argv[1:]) == 0:
        print('no file name specified')
        sys.exit(1)

    with open(sys.argv[1]) as f:
        filecontent = f.read()

    from configparser import ConfigParser
    conf = ConfigParser()
    confdefaults = b"""
[test]
dbconnectstring=
minattachmentsize=0
maxattachmentsize=1000000000000
archivecontentmaxsize=1000000000000
"""
    confdefaults = confdefaults.decode('utf-8','ignore') # only needed if using dummy "force_uString"
    conf.read_string(force_uString(confdefaults))

    s = SSDEEPCheck(conf, 'test')
    with patch('__main__.get_session', side_effect=get_session_mock):
        logger.debug("Output from scan_stream for viruses: %s" % str(s.scan_stream(filecontent)))

