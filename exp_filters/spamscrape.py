# -*- coding: UTF-8 -*-

import re

from fuglu.shared import ScannerPlugin, DUNNO, Suspect
from domainmagic import extractor, tld as tldextractor


class SpamScrape(ScannerPlugin):
    """
    This plugin checks for mails from a given domain regex
    and checks for links contained with a given tld.
    Mail can be marked as spam, links added to another tag
    which can be used by URIUDPSend
    """
    def __init__(self, config, section=None):
        super().__init__(config, section)
        self.logger = self._logger()
        
        self.requiredvars = {
            'suspect_tags': {
                'default': 'body.uris docx.hyperlinks pdf.hyperlinks',
                'description': 'uri tags to read & check'
            },

            'target_tag': {
                'default': 'scrape.uris',
                'description': 'target tag to be used by URIUDPSend'
            },

            'ham_only': {
                'default': 'false',
                'description': 'handle only messages not marked as spam yet'
            },

            'log_only': {
                'default': 'false',
                'description': 'log only without doing anything'
            },

            'senderdomainregex': {
                'default': '^(domain\.)',
                'description': 'regex to define sender domains to be handled'
            },

            'badtlds': {
                'default': 'invalid',
                'description': 'list (space separated) of tlds to list'
            },

            'mark_spam': {
                'default': 'true',
                'description': 'Mark message as spam on hit'
            },
        }

        self.tldmagic = tldextractor.TLDMagic()
        self.ham_only = self.config.getboolean(self.section, 'ham_only')
        self.log_only = self.config.getboolean(self.section, 'log_only')
        self.target_tag = self.config.get(self.section, 'target_tag')
        self.mark_spam = self.config.getboolean(self.section, 'mark_spam')
        self.badtlds = [tld.strip().lower() for tld in self.config.get(self.section, 'badtlds').split() if tld.strip()]

    def _process_sender(self, suspect: Suspect):
        senderdomain = self.config.get(self.section, 'senderdomainregex')
        if not suspect.from_domain:
            self.logger.debug(f"{suspect.id} empty from domain -> skip")
            return False
        elif not senderdomain:
            self.logger.error(f"{suspect.id} empty sender regex -> skip")
            return False
        elif not re.match(senderdomain, suspect.from_domain, re.IGNORECASE):
            self.logger.debug(f"{suspect.id} no regex match -> skip")
            return False
        return True

    def _get_uris(self, suspect):
        uris = []
        tags = self.config.get(self.section,'suspect_tags').split()
        for tag in tags:
            uris.extend(suspect.get_tag(tag, []))
        return uris

    def _get_tld_from_uri(self, uri, suspect=None):
        exclude_fqdn = ['www.w3.org', 'schemas.microsoft.com']
        exclude_domain = ['avast.com']

        suspectid = suspect.id if suspect else '<>'
        try:
            fqdn = extractor.domain_from_uri(uri)
            if fqdn in exclude_fqdn:
                return None
        except Exception as e:
            # log error
            self.logger.error(f"{suspectid} msg: {str(e)} uri: {uri}")
            return None

        try:
            domain = self.tldmagic.get_domain(fqdn)
        except Exception as e:
            # log error
            self.logger.error(f"{suspectid} msg: {str(e)} fqdn: {fqdn}")
            return None

        if domain in exclude_domain:
            return None

        # check if domain is actually an ip
        ip = extractor.is_ip(domain)
        if ip:
            return None

        tld = self.tldmagic.get_tld(domain) if domain else ""

        if bool(domain) and bool(tld):
            return tld
        else:
            return None

    def examine(self, suspect: Suspect):
        # check if already marked as spam and if we still want to process it
        if self.ham_only and not suspect.is_ham():
            self.logger.info(f"{suspect.id} skip pluging because message is non-ham already")

        # check if based on sender domain if mail should be processed
        if not self._process_sender(suspect=suspect):
            return DUNNO
        uris = self._get_uris(suspect)

        scrapeuris = []
        for uri in uris:
            tld = self._get_tld_from_uri(uri, suspect=suspect)
            if tld and tld.lower() in self.badtlds:
                scrapeuris.append(uri)

        if self.target_tag:
            if self.log_only:
                for suri in scrapeuris:
                    self.logger.debug(f"{suspect.id} -> scrape uri: {suri}")
            else:
                suspect.set_tag(self.target_tag, scrapeuris)

                if len(scrapeuris) > 2:
                    self.logger.info(f"{suspect.id} -> to tag {self.target_tag}:  {scrapeuris[:2]} + {len(scrapeuris) -2} more...")
                elif len(scrapeuris) > 0:
                    self.logger.info(f"{suspect.id} -> to tag {self.target_tag}:  {scrapeuris}")
        else:
            self.logger.error("No target tag defined!")

        isspam = len(scrapeuris) > 0

        if self.mark_spam and not self.log_only:
            suspect.tags['spam']['SpamScrape'] = isspam
            if isspam:
                self.logger.info(f"{suspect.id} mark as spam")

    def lint(self):
        """lint check"""
        if not self.check_config():
            return False

        if not self.target_tag:
            print('ERROR: no "target_tag" defined, plugin can not work')
            return False

        if not self.badtlds:
            print('ERROR: no "bad_tlds" defined, plugin can not work')
            return False

        senderdomain = self.config.get(self.section, 'senderdomainregex')
        if not senderdomain:
            print('ERROR: no "senderdomainregex" defined, plugin can not work')
            return False

        try:
            _ = re.compile(senderdomain, re.IGNORECASE)
        except Exception as e:
            print(f'ERROR: compiling senderdomainregex: {e}')

        if self.log_only:
            print('INFO: log only mode')

        return True
